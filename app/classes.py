from app import db


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    message = db.Column(db.String(3000), unique=True, nullable=False)

    def __str__(self):
        return 'name = {}, email = {}, message = {}'.format(name, email, message)