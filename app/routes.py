from app import app, db
from flask import render_template, flash, redirect, url_for, request
from app.forms import *


@app.route('/')
def main():
    form = ContactForm()
    return render_template('main.html', title='Home', form=form)
